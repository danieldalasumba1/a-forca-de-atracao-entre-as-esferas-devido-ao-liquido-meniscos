### Ponte liquida entre duas esferas
## Força de atração entre as esferas devido ao liquido meniscos 

A força de atração entre duas esferas é devido ao menisco liquido é dada pela soma da tensão superficial e sucção,quando estimamos a força no pescoço da ponte.

$`F_{bridge} = 2 \pi r_2 \gamma+ \pi r_2^2 \Delta P`$

Já a diferença de pressão entre o liquido e o ar é dada pela formula.


$`\Delta P = \gamma \left(\frac{1}{r_1} -\frac{1}{r_2}\right)`$

Caso 1

Para 
$`\gamma=0,5 g/m^2 `$

Quando $`r_2 > r_1`$

$`r_1=1 mm`$
$`r_2=1,5 mm`$

Teremos: 

$`\Delta P = 0,5(1/0,5-1/1,5)=0,4atm `$

$`\Delta P>0`$
Então:

$`F_{bridge} = 2*3,14*1,5*0,5+3,14*1,5.1,5*0,38= 7,4 N`$

Caso 2

Para 

$`\gamma =0,9 g/m^2`$

Quando $`r_2>r_1`$


$`r_1 = 1 mm`$
$`r_2 = 1,5 mm`$

Teremos:
$`\Delta P = 0,9(1/1-1/1,5) = 0,297atm`$

$`\Delta P>0`$

Então:

$`F_{bridge} = 2*3,14*1,5*0,9 + 3,14*1,5*1,5*0,297 = 9,9 N`$

Caso 3

$`\gamma = 1g/m^2`$

Quando $`r_2>r_1`$

$`r_1 = 1 mm`$
$`r_2 = 1,5 mm`$

Teremos:
$`\Delta P = 1*(1/1-1/1,5) = 0,3atm`$

$`\Delta P>0 `$

$`F_{bridge} = 2*3,14*1*1,5 + 3,14*1,5*1,5*0,3 = 11,5 N`$

Caso 4
$`\gamma = 2g/m^2`$

Quando $`r_1 > r_2`$

$`r_1 = 1 mm`$
$`r_2 = 1,5 mm`$

Teremos:
$`\Delta P = 2(1/1-1/1,5) = 0,66 atm`$

$`\Delta P>0`$

$`F_{bridge} = 2*3,14*2*1,5 +3,14*1,5*1,5*2 = 32,0 N`$ 

Caso 5 

$`\gamma = 5g/m^2`$

Quando $` r_1 > r_2`$

$`r_1 = 1 mm`$
$`r_2 = 1,5 mm`$
Teremos:

$`\Delta P = 5(1/1-1/1,5) = 1,7atm`$
$`\Delta P>0`$

Então:

$`F_{bridge} = 2*3,14*1,5*5 + 3,14*1,5*1,5*1,7 = 69,1 N`$

Caso 6

$`\gamma = 5,4g/m^2`$

$`r_1 = 1mm`$
$`r_2 = 1,5mm`$
$`r_1 > r_2`$

$`\Delta P = 5,4(1/1-1/1,5) = 1,8 atm`$

$`\Delta P>0`$

$`F_{bridge} = 2*3,14*5,4*1,5+3,14*1,5*1,5*1,8 = 63,5 N`$

Chegamos então a conclusão que quanto maior $`\gamma`$ maior será a força,e a avariação de pressão.

Podemos chegar na mesma conclusão com os mesmos dados pelo python a partir do gráfico gerado:


<img src="/Img.png" width="600">

